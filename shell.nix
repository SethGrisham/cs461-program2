{
  tinypkgs ? import (fetchTarball {
    url = "https://gitlab.inria.fr/nix-tutorial/packages-repository/-/archive/master/packages-repository-8e43243635cd8f28c7213205b08c12f2ca2ac74d.tar.gz";
    sha256 = "sha256:09l2w3m1z0308zc476ci0bsz5amm536hj1n9xzpwcjm59jxkqpqa";
  }) {}
}:

with tinypkgs; # Put tinypkgs's attributes in the current scope.
with pkgs; # Same for pkgs.

mkShell {
  buildInputs = [
    chord

    # Defines a python + set of packages.
    (python3.withPackages (ps: with ps; with python3Packages; [
      jupyter
      ipython

      python-lsp-server
      python-lsp-black
      numpy
      progress
      (
        buildPythonPackage rec {
          pname = "pygad";
   	  version = "3.2.0";
  	  src = fetchPypi {
  	    inherit pname version;
	    sha256 = "5bd4c70bc8c810c254ac145d3e4b1c0402f720ce2ce4a5e91be360c026644ea0";
	  };
	  doCheck = false;
	  propagatedBuildInputs = [
	    pkgs.python3Packages.numpy
	    pkgs.python3Packages.cloudpickle
	    pkgs.python3Packages.matplotlib
	  ];
        }
      )
    ]))
  ];

  # Automatically run jupyter when entering the shell.
  shellHook = "jupyter notebook";
}